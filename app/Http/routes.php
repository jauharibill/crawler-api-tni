<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// $app->get('/', function () use ($app) {
//     return $app->version();
// });

$app->get('/','CrawlerController@home');
$app->get('/fungsilogistik','CrawlerController@getFungsiLogistik');
$app->get('/jenislogistik','CrawlerController@getJenisLogistik');
$app->get('/logistik','CrawlerController@getLogistik');
$app->get('/kotama','CrawlerController@getKotama');
$app->get('/operasi','CrawlerController@getOperasi');
$app->get('/perbatasan','CrawlerController@getPerbatasan');
$app->get('/pulauterluar','CrawlerController@getPulauTerluar');
$app->get('/satminkal','CrawlerController@getSatminkal');
$app->get('/corpspersonel','CrawlerController@getCorpsPersonel');
$app->get('/pangkatpersonel','CrawlerController@getPangkatPersonel');
$app->get('/personel','CrawlerController@getPersonel');
$app->get('/desa','CrawlerController@getDesa');